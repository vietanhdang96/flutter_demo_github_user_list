import 'package:flutter/material.dart';
import 'package:flutter_demo_github_user_list/user.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _userList = <User>[];
  final String dataUrl = "https://api.github.com/orgs/raywenderlich/members";

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
          itemCount: _userList.length,
          itemBuilder: (BuildContext context, int index) {
            if (index.isOdd) return new Divider();
            return _buildRow(index ~/ 2);
          }),
    );
  }

  Widget _buildRow(int index) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListTile(
        title: new Text(_userList[index].login, style: TextStyle(fontSize: 15)),
        leading: new CircleAvatar(
          backgroundColor: Colors.blue,
          backgroundImage: new NetworkImage(_userList[index].avatarUrl),
        ),
      ),
    );
  }

//  void _loadData() async {
//    final http.Response response = await http.get(dataUrl);
//    setState(() {
//      final usersJSON = json.decode(response.body);
//      for (var userJSON in usersJSON) {
//        final user = new User(userJSON["login"], userJSON["avatar_url"]);
//        _userList.add(user);
//      }
//    });
//  }

  void _loadData() {
    http.get(dataUrl).then((http.Response response) {
      setState(() {
        final usersJSON = json.decode(response.body);
        for (var userJSON in usersJSON) {
          final user = new User(userJSON["login"], userJSON["avatar_url"]);
          _userList.add(user);
        }
      });
    }, onError: (e) {});
  }
}
